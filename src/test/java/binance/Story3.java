package binance;

import cryptoCurrency.CryptoCurrency;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.gradle.internal.impldep.org.junit.Assert.assertEquals;

public class Story3 {
    private BinanceImplementation binance;
    CryptoCurrency cryptoCurrency;

    @Spy
    BinanceImplementation binanceFake;

    @Before
    public void init() {
        binance = new BinanceImplementation();
        cryptoCurrency = new CryptoCurrency("BITCOIN","BTC");
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetPriceTimeoutExceeded()
    {
        binance.setTimeout(1);
        assertEquals(binance.getPrice("BTC"), -1, 0);
    }

    @Test
    public void testGetPrice()
    {
        Mockito.when(binanceFake.getPrice("BTC")).thenReturn(100.0);
        assertEquals(binanceFake.getPrice("BTC"), 100, 0);
    }

    @Test
    public void testGetPriceFail()
    {
        Mockito.when(binanceFake.getPrice("BTC")).thenReturn(-1.0);
        assertEquals(binanceFake.getPrice("BTC"), -1.0, 0);
    }

}
