package binance;

import cryptoCurrency.CryptoCurrency;
import operation.OperationType;
import operation.Order;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.util.Date;

import static org.gradle.internal.impldep.org.junit.Assert.*;

public class BinanceTests {
    private BinanceImplementation binance;
    private CryptoCurrency cryptoCurrency;


    @Before
    public void init() {
        binance = new BinanceImplementation();
        cryptoCurrency = new CryptoCurrency("BITCOIN","BTC");
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testPlaceOrder()
    {
        Order order = new Order(100.0,new Date(), 1.0, cryptoCurrency, OperationType.SELL);
        assertTrue(!binance.placeOrder(order).equals(""));
    }
    @Test
    public void testGetName()
    {
        assertTrue(binance.getName().equals("BINANCE"));
    }
}
