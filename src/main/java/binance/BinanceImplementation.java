package binance;

import com.google.common.base.Charsets;
import com.google.common.io.ByteSource;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import market.Market;
import operation.Order;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class BinanceImplementation implements Market {
	private final String name = "BINANCE";
	private final String binanceUrl = "https://api.binance.com/api/v3";
	private final String ticketPrice = "/ticker/price?symbol=";
	private final String resultingPair = "USDT";
	private int timeout = 60000;

	@Override
	public double getPrice(String symbol) {
		HttpURLConnection con;
		String result = "";
		try {
			con = createConnection(symbol);
			con.setConnectTimeout(timeout);
			con.setReadTimeout(timeout);
			result = inputStreamToString(con.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return !result.isEmpty() ? extractPrice(result) : -1;
	}

	private String inputStreamToString(InputStream inputStream) throws IOException {
		ByteSource byteSource = new ByteSource() {
			@Override
			public InputStream openStream() throws IOException {
				return inputStream;
			}
		};
		return byteSource.asCharSource(Charsets.UTF_8).read();
	}

	private HttpURLConnection createConnection(String symbol) throws IOException {
		URL url = new URL(binanceUrl + ticketPrice + symbol + resultingPair);
		return (HttpURLConnection) url.openConnection();
	}

	private double extractPrice(String response) {
		JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();
		return (jsonObject.get("price") != null) ? jsonObject.get("price").getAsDouble() : -1;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String placeOrder(Order order) {
		return generateCode();
	}

	private String generateCode() {
		StringBuilder orderCode = new StringBuilder();
		for (int i = 0; i < 3; i++) {
			orderCode.append(generateRandomLetter()).append(generateRandomNumber());
		}
		return orderCode.toString();
	}

	private String generateRandomNumber() {
		Random r = new Random();
		return String.valueOf(r.nextInt(26));
	}

	private String generateRandomLetter() {
		Random r = new Random();
		return String.valueOf(r.nextInt(26) + 'a');
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
}
